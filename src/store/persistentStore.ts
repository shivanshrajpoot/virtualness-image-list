import create, { StateCreator } from 'zustand';
import { persist } from 'zustand/middleware';
import pipe from 'ramda/es/pipe';

interface OptionsType {
    name: string;
}

const configurePersistentStore = <T>(persistorName: string) => {
    const options: OptionsType = {
        name: persistorName,
    };
    const persistor = (store: StateCreator<T>) => persist(store, options);
    return pipe(persistor, create);
};

export default configurePersistentStore;
