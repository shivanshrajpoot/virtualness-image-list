import { ImageListItem } from '../services/api';
import configurePersistentStore from './persistentStore';
import { produce } from 'immer';

export interface TitleListStore {
    titleList: {
        [itemId: string]: string;
    };
    addItem: (item: ImageListItem) => void;
}

const createStore = configurePersistentStore<TitleListStore>('titleListStore');

const useTitleListStore = createStore((set, get) => ({
    titleList: {},
    addItem: (item: ImageListItem) => {
        set(
            produce((state: TitleListStore) => {
                const stateTitleList = state.titleList;
                state.titleList = {
                    ...stateTitleList,
                    [item.id]: item.title,
                };
            }),
        );
    },
    removeItem: (item: ImageListItem) => {
        set(
            produce((state: TitleListStore) => {
                delete state.titleList[item.id];
            }),
        );
    },
}));

export default useTitleListStore;
