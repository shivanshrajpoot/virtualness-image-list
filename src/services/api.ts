const ENDPOINT = 'https://6373dc9c716c2e19165311e6.mockapi.io/virtualness';

export type ImageListItem = {
    createdAt: number;
    title: string;
    imageUrl: string;
    id: string;
};

export type GetImageResponse = ImageListItem[];

export const getImages = async () => {
    const res = await fetch(`${ENDPOINT}/image`);
    return (await res.json()) as Promise<GetImageResponse>;
};
