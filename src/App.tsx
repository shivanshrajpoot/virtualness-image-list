import * as React from "react";
import ImageList from "./components/ImageList";
import { TitleList } from "./components/TitleList";
import { getImages, ImageListItem } from "./services/api";
import useTitleListStore from "./store/titleListStore";
import './style.css';


export default () => {
    const [listData, setListData] = React.useState<ImageListItem[]>([]);
    React.useEffect(() => {
        getImages().then((res) => {
            setListData(res)
        })
    }, []);
    return (
        <>
            <h1>
                List App
            </h1>
            <TitleList />
            <ImageList listData={listData} />
        </>
    )
}