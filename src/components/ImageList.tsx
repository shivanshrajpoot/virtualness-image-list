import * as React from "react"
import { ImageListItem } from "../services/api"
import useTitleListStore from "../store/titleListStore"
import ListItem from "./ImageListItem"

export interface ImageListPropsI {
    listData: ImageListItem[]
}

const ImageList = ({ listData }: ImageListPropsI) => {
    const addItem = useTitleListStore(state => state.addItem)
    const addItemToList = React.useCallback((item: ImageListItem) => {
        addItem(item)
    }, [listData])
    console.log("rerendering Image list")
    return (
        <div className="row">
            {listData.map.length > 0 && listData.map((item) =>
                <ListItem item={item} key={item.id} addItemToList={addItemToList} />)
            }
        </div>
    )
}

export default ImageList;
