import * as React from "react";
import { ImageListItem } from "../services/api";
import useTitleListStore, { TitleListStore } from "../store/titleListStore";

export interface ImageListItemPropsI {
    item: ImageListItem;
    addItemToList: (item: ImageListItem) => void;
}

const getHeightFromUrl = (url: string) => {
    return url.split('/').slice(-1)[0]
}

const getWidthFromUrl = (url: string) => {
    return url.split('/').slice(-2)[0]
}

const ListItem = ({ item, addItemToList }: ImageListItemPropsI) => {
    console.log("rerendering Image item")
    return (
        <div onClick={() => addItemToList(item)} className="column">
            <img
                loading="lazy"
                className="itemImage"
                src={item.imageUrl}
                alt={item.title}
                width={getWidthFromUrl(item.imageUrl)}
                height={getHeightFromUrl(item.imageUrl)} />
            <h2 className="itemTitle">{item.title}</h2>
        </div>
    )
}

export default (ListItem);