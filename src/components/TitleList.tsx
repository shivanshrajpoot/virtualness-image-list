import * as React from "react"
import useTitleListStore, { TitleListStore } from "../store/titleListStore";
import shallow from 'zustand/shallow';

export const TitleList = () => {
    const titleList = useTitleListStore((state: TitleListStore) => state.titleList, shallow);

    console.log("rerendering title list")
    return (
        <div>
            {Object.keys(titleList).map((key: string) => <p key={key}>{titleList[key]}</p>)}
        </div>
    )
}

export default (TitleList);